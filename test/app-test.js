/* globals describe, context, it */
'use strict';
import chai from 'chai';
import { expect } from 'chai';
import spies from 'chai-spies';
chai.use(spies);

import MainClass from '../src/app.js';

const app = new MainClass();

describe('App', () => {
  it('Should bootstrap', () => {
    let spy = chai.spy.on(app, 'bootstrap');
    
    app.bootstrap();

    expect(spy).to.have.been.called();
  });
});
